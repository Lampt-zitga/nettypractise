package practise;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.nio.charset.Charset;
import java.util.Scanner;

public class ClientDeliverer extends ChannelInboundHandlerAdapter{

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        while(true){
            System.out.println("Channel read deliverer");
            Scanner sc=new Scanner(System.in);
            String mess=sc.nextLine();
            ByteBuf byteBuf1= Unpooled.copiedBuffer(mess, Charset.forName("UTF-8"));
            ChannelFuture future=ctx.writeAndFlush(byteBuf1);
        }
    }

}
