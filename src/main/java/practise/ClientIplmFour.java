package practise;

public class ClientIplmFour {
    public static void main(String[] args) {
        new Thread(()->{
            new ClientDelivererApp("localhost",8100).run();
        }).start();
        new Thread(()->{
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            new ClientRecieverApp("localhost",8100).run();
        }).start();
    }
}
