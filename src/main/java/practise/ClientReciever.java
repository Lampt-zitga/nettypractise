package practise;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.nio.charset.Charset;

public class ClientReciever extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("channel read receiver");
        ByteBuf byteBuf2=(ByteBuf) msg;
        System.out.println(byteBuf2.toString(Charset.forName("UTF-8")));
    }
}
