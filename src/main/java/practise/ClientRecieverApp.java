package practise;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class ClientRecieverApp {
    private String host;
    private int port;

    public ClientRecieverApp(String host, int port){
        this.host=host;
        this.port=port;
    }

    public void run(){
        NioEventLoopGroup loopGroup=new NioEventLoopGroup();
        try{
            Bootstrap cbt=new Bootstrap();
            cbt.group(loopGroup).channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            socketChannel.pipeline()
                                    .addLast(new ClientReciever());
                        }
                    }).option(ChannelOption.SO_KEEPALIVE,true);

            ChannelFuture cf=cbt.connect(host,port).sync();
            cf.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }  finally {
            loopGroup.shutdownGracefully();
        }
    }
}
