package practise;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.net.SocketAddress;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class EchoServerHandler extends ChannelInboundHandlerAdapter {

    public static ChannelGroup channels=new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    public static Map<SocketAddress,Integer> listSk=new HashMap<>();
    public static Integer socketNID=1;
    public static int socketCount=1;
    
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Channel incomming=ctx.channel();
        System.out.println("receive from "+incomming.remoteAddress());
        int icmChannelId=listSk.get(incomming.remoteAddress());
        ByteBuf byteBuf=(ByteBuf) msg;
        String s="<"+incomming.remoteAddress()+">: "+byteBuf.toString(Charset.forName("UTF-8"));
        for(Channel channel:channels){
                int channelId=listSk.get(channel.remoteAddress());
                if(channelId!=icmChannelId){
                    System.out.println("send to "+channel.remoteAddress());
                    channel.writeAndFlush(Unpooled.copiedBuffer(s,Charset.forName("UTF-8")));
                }
        }
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        System.out.println("handler added server");
        Channel incomming=ctx.channel();
        if(socketCount%2==1){
            listSk.put(incomming.remoteAddress(),socketNID);
            socketCount+=1;
        }else{
            listSk.put(incomming.remoteAddress(), socketNID);
            socketCount+=1;
            socketNID+=1;
        }
        String msg="<"+incomming.remoteAddress()+"> joined";
        channels.writeAndFlush(Unpooled.copiedBuffer(msg,Charset.forName("UTF-8")));
        channels.add(ctx.channel());
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        System.out.println("handler remove server");
        Channel incomming=ctx.channel();
        String msg="<"+incomming.remoteAddress()+"> left\r\n";
        channels.writeAndFlush(Unpooled.copiedBuffer(msg,Charset.forName("UTF-8")));
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("channel active server");
        Channel channel= ctx.channel();
        System.out.println("Channel "+channel.remoteAddress()+" active");

    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("channel inactive server");
        Channel channel= ctx.channel();
        System.out.println("Channel "+channel.remoteAddress()+" inactive");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println("exception");
        cause.printStackTrace();
        ctx.close();
    }
}
